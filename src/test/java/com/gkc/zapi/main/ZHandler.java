package com.gkc.zapi.main;

import java.security.KeyPairGeneratorSpi;
import java.util.HashMap;

public class ZHandler {

    private static ZHandler zHandler;
    private static ZapiTest zTest;

    public static ZHandler getInstance() {
        if(zHandler == null){
            zHandler = new ZHandler();
            zTest = new ZapiTest();
        }
        return zHandler;
    }

    public void initialize(HashMap<String, String> data) {
        zTest.initializeTest(data);
    }

    public void handleCycleCreation(String env, String cycleName){
        zTest.updateVersionId();
    }
}
