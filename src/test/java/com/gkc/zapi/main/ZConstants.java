package com.gkc.zapi.main;

public interface ZConstants {
    int HTTP_OK=200;
    String PROJECT_ID = "projectId";
    String VERSION_NAME = "version_name";
    String COMPONENT = "component";
    String ENV = "env";
    String PROJECT_IDENTIFIER = "$PROJECTID$";
}
