package com.gkc.zapi.main;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class ZConcurrentEventListener implements ConcurrentEventListener {



    @Override
    public void setEventPublisher(EventPublisher publisher) {
        publisher.registerHandlerFor(TestRunStarted.class, testStartedHandler);
        publisher.registerHandlerFor(TestCaseStarted.class, testExecutionHandler);
        publisher.registerHandlerFor(TestStepFinished.class, testStepFinishedHandler);
        publisher.registerHandlerFor(TestCaseFinished.class, testCaseFinishedHandler);
    }

    private String componentName = "ms-token-management";
    private String env = "SIT";
    String username = "A9001221";
    String password = "";

    private EventHandler<TestRunStarted> testStartedHandler = new EventHandler<TestRunStarted>() {
        @Override
        public void receive(TestRunStarted event) {
            HashMap<String, String> dat = new HashMap<>();
            dat.put("projectId". "10601");
            dat.put("versionName", "10601");
            dat.put("component", componentName);
            dat.put("sprintName", "PDIG Sprint 34 - SGCN");
            dat.put("boardId", "721");
            dat.put("env", env);
            dat.put("transitionId", "91");
            dat.put("password", password);
            dat.put("username", username);
            ZHandler.getInstance().initialize(dat);

            LocalDateTime datetime = LocalDateTime.ofInstant(event.getInstant(), ZoneOffset.systemDefault());
            String formattedDt = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(datetime);
            String folderName = componentName + "_" + formattedDt;

            try{
                ZHandler.getInstance().handleCycleCreation(env, folderName);
            } catch (Exception exception) {
                System.out.println("Failed with exception "+ exception.getMessage());
                System.exit(0);
            }


        }
    };
}
