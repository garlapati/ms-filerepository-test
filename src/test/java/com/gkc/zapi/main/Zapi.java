package com.gkc.zapi.main;

public interface Zapi {
    String createTestIssueURL= "https://jira.ocbc.com/rest/api/2/issue";
    String createTestStepURL = "https://jira.ocbc.com/rest/zapi/latest/teststep/";
    String createTestCycleURL = "https://jira.ocbc.com/rest/zapi/latest/cycle";
    String getCycleInfoURL = "https://jira.ocbc.com/rest/zapi/latest/cycle?projectId=$PROJECTID$&versionId=$VERSIONID$";


    String getAllVersion = "https://jira.ocbc.com/rest/api/2/project/$PROJECTID$/versions";
}
