package com.gkc.zapi.main;

import io.restassured.builder.RequestSpecBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ZapiTest {

    private ArrayList<String> steps;
    private HashMap<String, Integer> statusMap;
    private int stepId;
    private String projectId;
    private String versionName;
    private String componentName;
    private String testEnvironment;
    private String assignee;
    private String password;


    public ZapiTest(){
        steps = new ArrayList<>();
        statusMap = new HashMap<>();
        statusMap.put("PASSED", 1);
        statusMap.put("FAILED", 2);
        stepId = -1;
    }

    public void initializeTest(Map map){
        this.projectId = map.get(ZConstants.PROJECT_ID).toString();
        this.versionName = map.get(ZConstants.VERSION_NAME).toString();
        this.componentName = map.get(ZConstants.COMPONENT).toString();
        this.testEnvironment = map.get(ZConstants.ENV).toString();


    }

    public void updateVersionId() {
        boolean foundVersion = false;
        Zapi.getAllVersion
                .replace(ZConstants.PROJECT_IDENTIFIER, projectId);

        request("", url, "GET");
    }

    public void request(String body, String service, String method){
        System.out.println(body);
        RequestSpecBuilder rsb = new RequestSpecBuilder();
        rsb.addHeader("Authorization", getBasicAuth(this.assignee, this.password));
    }

    private String getBasicAuth(String cid, String cpass) {
        if(apiKey == null)
        return null;
    }
}
