package com.gkc.zapi.bean;

public class Folder {

    private String cycleId;
    private String description;
    private String projectId;
    private String versionId;
    private String name;

    public Folder(String cycleId,  String projectId, String versionId, String name) {
        this.cycleId = cycleId;
        this.projectId = projectId;
        this.versionId = versionId;
        this.name = name;
    }

    public String getCycleId() {
        return cycleId;
    }

    public String getDescription() {
        return "folder for execution cycle : "+getCycleId();
    }

    public String getProjectId() {
        return projectId;
    }

    public String getVersionId() {
        return versionId;
    }

    public String getName() {
        return name;
    }
}
