package com.gkc.zapi.bean;

import java.util.ArrayList;

public class Fields {
    private Project project;
    private String summary;
    private String description;
    private IssueType issuetype;

    private int customfield_10105; //sprint id
    private ArrayList<Components> components;
    private Assignee assignee;

    public Fields() {
        project = new Project();
        components = new ArrayList<>();
        issuetype = new IssueType();
        assignee = new Assignee();
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getCustomfield_10105() {
        return customfield_10105;
    }

    public void setCustomfield_10105(int sprintId) {
        this.customfield_10105 = sprintId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IssueType getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(IssueType issuetype) {
        this.issuetype = issuetype;
    }

    public ArrayList<Components> getComponents(){
        return components;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }
}
