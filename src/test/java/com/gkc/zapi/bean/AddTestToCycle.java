package com.gkc.zapi.bean;

public class AddTestToCycle {

    private String cycleId;
    private String issueID;
    private String assigneeType;
    private String assignee;
    private String projectId;
    private String versionId;
    private String folderId;

    public String getCycleId() {
        return cycleId;
    }

    public String getIssueID() {
        return issueID;
    }

    public String getAssigneeType() {
        return "assignee";
    }

    public String getAssignee() {
        return assignee;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getVersionId() {
        return versionId;
    }

    public String getFolderId() {
        return folderId;
    }

    public String getDescription(){
        return "folder for execution cycle: "+ cycleId;
    }
}
